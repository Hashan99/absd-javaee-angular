import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {CustomerDTO} from '../dto/CustomerDTO';

export const MAIN_URL = 'http://localhost:8081';
const CUSTOMER_URL = '/api/customer';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private httpclient: HttpClient) {
  }

  getAllCustomer(): Observable<Array<CustomerDTO>> {
    return this.httpclient.get<Array<CustomerDTO>>(MAIN_URL + CUSTOMER_URL);
  }

  addCustomer(customer: CustomerDTO): Observable<boolean> {
    return this.httpclient.post<boolean>(MAIN_URL + CUSTOMER_URL, customer);
  }

  editCustomer(customer: CustomerDTO): Observable<boolean> {
    return this.httpclient.put<boolean>(MAIN_URL + CUSTOMER_URL, customer);
  }

  deleteCustomer(customer: CustomerDTO): Observable<boolean> {
    return this.httpclient.delete<boolean>(MAIN_URL + CUSTOMER_URL + '?id=' + customer.id);
  }
}
