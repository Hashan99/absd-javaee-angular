import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, pipe, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable()
export class DataService {

  constructor(
    private httpClient: HttpClient) {
  }

  public request(conf: any, data?: any): Observable<any> {
    if (!data) {
      data = {};
    }
    if (conf.type === 'POST') {
      return this.httpClient.post(conf.url, data).pipe(
        map((t: any) => {
          return (t);
        }),
        catchError(error => {
          console.log(error);
          return throwError('Request Failed');
        }));
    } else {
      return this.httpClient.get(conf.url, {
        params: Object.entries(data).reduce(
          (params, [Key, value]) => params.set(Key, '' + value), new HttpParams())
      }).pipe(
        map((t: any) => {
          return (t);
        }),
        catchError(error => {
          console.log(error);
          return throwError('Request Failed');
        }));
    }
  }
}
