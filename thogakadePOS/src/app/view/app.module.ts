import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponentComponent} from './home-component/home-component.component';
import {OrderModule} from './order/order.module';
import {ItemModule} from './item/item.module';
import {DashbordModule} from './dashbord/dashbord.module';
import {DataService} from '../service/DataService';
import {ItemService} from '../service/ItemService';
import {CustomerService} from '../service/CustomerService';
import {HttpClientModule} from '@angular/common/http';
import {CustomerModule} from './customer/customer.module';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponentComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    OrderModule,
    ItemModule,
    DashbordModule,
    CustomerModule,
    FormsModule
  ],
  providers: [DataService, ItemService, CustomerService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
