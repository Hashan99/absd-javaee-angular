import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerFormComponent } from './customer-form/customer-form.component';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [CustomerFormComponent],
  imports: [
    CommonModule,
      FormsModule
  ]
})
export class CustomerModule { }
