import {Component, OnInit} from '@angular/core';
import {CustomerDTO} from '../../../dto/CustomerDTO';
import {CustomerService} from '../../../service/CustomerService';


@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  customer: CustomerDTO[] = [];
  id: string;
  name: string;
  address: string;

  constructor(private customerService: CustomerService) {
  }

  ngOnInit() {
    this.getAllCustomer();
  }

  private getAllCustomer() {
    this.customerService.getAllCustomer().subscribe(data => {
      this.customer = data;
    });
  }

  Save() {
    const cust = new CustomerDTO();
    cust.id = this.id;
    cust.name = this.name;
    cust.address = this.address;
    console.log(this.id);
    this.customerService.addCustomer(cust).subscribe(data => {
      if (data) {
        alert(' Customer is save');
      } else {
        alert('Customer save failed');
      }
    });
  }
  onSelect(selectedItem: any) {
    console.log('Selected item Id: ', selectedItem.Id);
  }
}
