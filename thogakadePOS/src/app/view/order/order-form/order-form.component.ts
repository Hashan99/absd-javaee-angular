import {Component, OnInit} from '@angular/core';
import {CustomerService} from '../../../service/CustomerService';
import {CustomerDTO} from '../../../dto/CustomerDTO';
import {ItemDTO} from '../../../dto/ItemDTO';

@Component({
  selector: 'app-order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.css']
})
export class OrderFormComponent implements OnInit {


  constructor() {
  }

  ngOnInit() {
  }

}
