import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponentComponent} from './home-component/home-component.component';
import {OrderFormComponent} from './order/order-form/order-form.component';
import {ItemFormComponent} from './item/item-form/item-form.component';
import {DashboardComponent} from './dashbord/dashboard/dashboard.component';
import {CustomerFormComponent} from './customer/customer-form/customer-form.component';

const routes: Routes = [
  {
    path: 'home', component: HomeComponentComponent, children: [
      {path: 'dashboard', component: DashboardComponent},
      {path: 'orderForm', component: OrderFormComponent},
      {path: 'itemForm', component: ItemFormComponent},
      {path: 'customerForm', component: CustomerFormComponent},
    ]
  },
  {path: '', pathMatch: 'full', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
