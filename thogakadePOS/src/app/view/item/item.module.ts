import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemFormComponent } from './item-form/item-form.component';

@NgModule({
  declarations: [ItemFormComponent],
  imports: [
    CommonModule
  ]
})
export class ItemModule { }
