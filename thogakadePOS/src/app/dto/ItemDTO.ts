export class ItemDTO {

  iid: string;
  itemName: string;
  unitPrice: number;
  qty: number;

}
